package com.incedo.rhpam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incedo.rhpam.dao.CandidateDao;
import com.incedo.rhpam.entity.Candidate;

@Service
public class CandidateServiceImpl implements CandidateService{

	@Autowired
	private CandidateDao dao;
	

	@Override
	public int saveCandidateDetails(Candidate candidate) {
		return dao.saveCandidate(candidate);
	}

}
