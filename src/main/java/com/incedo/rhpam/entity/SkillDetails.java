package com.incedo.rhpam.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="skill_details")
public class SkillDetails implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1812259840967434650L;
	@Id
	@Column(name="skill_id")
	private int skillId;
	@Column(name="skill_name")
	private String skillName;
	@Column(name="skill_exp")
	private String skillExp;
	@Column(name="skill_desc")
	private String skillDesc;
	//@Column(name="candidate_id")
	@ManyToOne
	@JoinColumn(name="candidate_id", nullable=false)
	private Candidate candidate;
	
	
	
	public int getSkillId() {
		return skillId;
	}
	public void setSkillId(int skillId) {
		this.skillId = skillId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public String getSkillExp() {
		return skillExp;
	}
	public void setSkillExp(String skillExp) {
		this.skillExp = skillExp;
	}
	public String getSkillDesc() {
		return skillDesc;
	}
	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}
	public Candidate getCandidate() {
		return candidate;
	}
	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}



}
