package com.incedo.rhpam.validation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.incedo.rhpam.utility.Gender;
import com.incedo.rhpam.utility.Roles;
import com.incedo.rhpam.utility.Status;
@NotNull(message = "User Request Required")
public class UserRequest {

	@NotNull(message = "Name cannot be null")
	@Size(min = 3, max = 50, message 
	= "first Name minimum 3 letters")
	@Pattern(regexp = "[a-z-A-Z]*", message = "First name has invalid characters")
	private String firstName;
	@NotNull(message = "Name cannot be null")
	@Size(min = 3, max = 50, message 
	= "last Name minimum 3 letters")
	@Pattern(regexp = "[a-z-A-Z]*", message = "Last name has invalid characters")
	private String lastName;
	@NotNull(message = "Gender Required")
	private Gender gender;
	@NotNull(message = "Role is Required")
	private Roles roles;
	@NotNull(message = "status is Required")
	private Status status;
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Roles getRoles() {
		return roles;
	}
	public void setRoles(Roles roles) {
		this.roles = roles;
	}
	
}
