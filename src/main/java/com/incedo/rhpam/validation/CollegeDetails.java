package com.incedo.rhpam.validation;

public class CollegeDetails {

	private String collegeName;
	private String collegeAddress1;
	private String collegeAddress2;
	private String collegeAddress3;
	private String collegeCity;
	private String collegeState;
	private String collegeCountry;
	private String collegePinCode;
	
	
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public String getCollegeAddress1() {
		return collegeAddress1;
	}
	public void setCollegeAddress1(String collegeAddress1) {
		this.collegeAddress1 = collegeAddress1;
	}
	public String getCollegeAddress2() {
		return collegeAddress2;
	}
	public void setCollegeAddress2(String collegeAddress2) {
		this.collegeAddress2 = collegeAddress2;
	}
	public String getCollegeAddress3() {
		return collegeAddress3;
	}
	public void setCollegeAddress3(String collegeAddress3) {
		this.collegeAddress3 = collegeAddress3;
	}
	public String getCollegeCity() {
		return collegeCity;
	}
	public void setCollegeCity(String collegeCity) {
		this.collegeCity = collegeCity;
	}
	public String getCollegeState() {
		return collegeState;
	}
	public void setCollegeState(String collegeState) {
		this.collegeState = collegeState;
	}
	public String getCollegeCountry() {
		return collegeCountry;
	}
	public void setCollegeCountry(String collegeCountry) {
		this.collegeCountry = collegeCountry;
	}
	public String getCollegePinCode() {
		return collegePinCode;
	}
	public void setCollegePinCode(String collegePinCode) {
		this.collegePinCode = collegePinCode;
	}
}
