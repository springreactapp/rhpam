package com.incedo.rhpam.validation;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.incedo.rhpam.utility.Gender;

@NotNull(message = "candidate Request Required")
public class CandidateRequest {

	@NotNull(message = "Name cannot be null")
	@Size(min = 3, max = 50, message 
	= "first Name minimum 3 letters")
	@Pattern(regexp = "[a-z-A-Z]*", message = "First name has invalid characters")
	private String firstName;
	@NotNull(message = "Name cannot be null")
	@Size(min = 3, max = 50, message 
	= "last Name minimum 3 letters")
	@Pattern(regexp = "[a-z-A-Z]*", message = "Last name has invalid characters")
	private String lastName;
	@NotNull
	private String mobileNumber;
	
	@NotNull(message = "candidate email is required")
	private String candidateemail;

	@NotNull
	private String candidateExp;
	@NotNull
	@Size(min=3,max=50,message="candidate address1 is required")
	private String candidateaddress1;
	@NotNull
	@Size(min=3,max=50,message="candidate address2 is required")
	private String candidateaddress2;
	@NotNull
	@Size(min=3,max=50,message="candidate address3 is required")
	private String candidateaddress3;
	@NotNull
	@Size(min=3,max=50,message="candidate address4 is required")
	private String candidateaddress4;
	@NotNull
	@Size(min=3,max=50,message="candidate city is required")
	private String candidateCity;
	@NotNull
	@Size(min=3,max=50,message="candidate state is required")
	private String candidateState;
	@NotNull
	@Size(min=3,max=50,message="candidate country is required")
	private String candidateCountry;
	@NotNull
	private Integer pin;
	
	@NotNull(message = "candidate Gender Required")
	private Gender gender;
	@NotNull
	private List<Skill> skills;
	
	@NotNull
	private List<PreviousCompanyDetails> previousCompanyDetails;
	
	@NotNull
	private List<CollegeDetails> collegerDetails;
	
	@NotNull
	private Resume resume;
	
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCandidateemail() {
		return candidateemail;
	}
	public void setCandidateemail(String candidateemail) {
		this.candidateemail = candidateemail;
	}
	public String getCandidateExp() {
		return candidateExp;
	}
	public void setCandidateExp(String candidateExp) {
		this.candidateExp = candidateExp;
	}
	public String getCandidateaddress1() {
		return candidateaddress1;
	}
	public void setCandidateaddress1(String candidateaddress1) {
		this.candidateaddress1 = candidateaddress1;
	}
	public String getCandidateaddress2() {
		return candidateaddress2;
	}
	public void setCandidateaddress2(String candidateaddress2) {
		this.candidateaddress2 = candidateaddress2;
	}
	public String getCandidateaddress3() {
		return candidateaddress3;
	}
	public void setCandidateaddress3(String candidateaddress3) {
		this.candidateaddress3 = candidateaddress3;
	}
	public String getCandidateaddress4() {
		return candidateaddress4;
	}
	public void setCandidateaddress4(String candidateaddress4) {
		this.candidateaddress4 = candidateaddress4;
	}
	public String getCandidateCity() {
		return candidateCity;
	}
	public void setCandidateCity(String candidateCity) {
		this.candidateCity = candidateCity;
	}
	public String getCandidateState() {
		return candidateState;
	}
	public void setCandidateState(String candidateState) {
		this.candidateState = candidateState;
	}
	public String getCandidateCountry() {
		return candidateCountry;
	}
	public void setCandidateCountry(String candidateCountry) {
		this.candidateCountry = candidateCountry;
	}
	public Integer getPin() {
		return pin;
	}
	public void setPin(Integer pin) {
		this.pin = pin;
	}
	public List<Skill> getSkills() {
		return skills;
	}
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	public List<PreviousCompanyDetails> getPreviousCompanyDetails() {
		return previousCompanyDetails;
	}
	public void setPreviousCompanyDetails(
			List<PreviousCompanyDetails> previousCompanyDetails) {
		this.previousCompanyDetails = previousCompanyDetails;
	}
	public List<CollegeDetails> getCollegerDetails() {
		return collegerDetails;
	}
	public void setCollegerDetails(List<CollegeDetails> collegerDetails) {
		this.collegerDetails = collegerDetails;
	}
	public Resume getResume() {
		return resume;
	}
	public void setResume(Resume resume) {
		this.resume = resume;
	}
}
