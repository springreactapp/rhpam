package com.incedo.rhpam.validation;

import org.springframework.web.multipart.MultipartFile;

public class Resume {

	private String resumeType;
	private MultipartFile multipartFile;
	
	
	public String getResumeType() {
		return resumeType;
	}
	public void setResumeType(String resumeType) {
		this.resumeType = resumeType;
	}
	public MultipartFile getMultipartFile() {
		return multipartFile;
	}
	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}
	
}
